require "byebug"


class Hangman
  attr_reader :guesser, :referee
  attr_accessor :board, :missed_guesses

  def initialize(players)
    @guesser = players[:guesser]
    @referee = players[:referee]
    @board = []
  end

  def setup
    length = referee.pick_secret_word
    guesser.register_secret_length(length)
    length.times {@board << '_'}
  end

  def take_turn
    got_guess = guesser.guess(@board)
    correct_letter_idx = referee.check_guess(got_guess)
    update_board(correct_letter_idx, got_guess)
    guesser.handle_response
  end

  def update_board(array, guess)
    array.each {|i| @board[i] = guess}
  end

end

class HumanPlayer



  def guess
    puts "make a guess"
    guess = gets.chomp
  end

  def register_secret_length(length)
    puts "The length of the secret word is #{length}"
  end

  def handle_response
  end

end

class ComputerPlayer

  attr_accessor :dictionary, :candidate_words

  def initialize(dictionary)
    @candidate_words = dictionary
    @missed_guesses = []
  end

  def candidate_words
    @candidate_words.select { |word| word.length == @secret_length }
  end


  def register_secret_length(length)
    @secret_length = length
  end

  def guess(board)
    candidate_letters = @candidate_words.join('')
    candidate_letters.delete!(@missed_guesses.join) unless @missed_guesses.empty?
    candidate_letters.delete!(board.join(''))
    candidate_letter_hash = Hash.new(0)
    candidate_letters.each_char { |letter| candidate_letter_hash[letter] += 1}
    candidate_letter_hash.select! {|k, v| v == candidate_letter_hash.values.max}
    candidate_letter_hash.keys.sample
  end

  def handle_response(letter, array)
    @missed_guesses << letter if array.empty?
    @candidate_words.each do |word|
      @candidate_words.delete(word) if word.include?(letter) && array.length == 0
      if word.count(letter) > array.size || word.count(letter) < array.size
        @candidate_words.delete(word)
      end
    end
    @candidate_words.each do |word|
      array.each do |idx|
        @candidate_words.delete(word) if word[idx] != letter
      end
    end
  end


  def secret_word
    idx = @candidate_words.length
    @candidate_words[rand(idx)]
  end

  def pick_secret_word
    secret_word.length
  end

  def check_guess(letter)
    idx_result = []
    (0...secret_word.length).select {|i| secret_word[i] == letter}
  end


end


# dictionary = File.readlines("dictionary.txt").map {|line| line.chomp}
